<?php

namespace app\controllers;

use app\controllers\bases\ActiveController;
use app\models\Task;
use Throwable;
use Yii;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\web\NotFoundHttpException;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends ActiveController
{
    /**
     * @var string
     */
    public $modelClass = Task::class;

    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        $actions                                 = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
        unset($actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }

    /**
     * @return ActiveDataProvider
     */
    public function prepareDataProvider(): ActiveDataProvider
    {
        $searchModel = new Task();

        return $searchModel->search(Yii::$app->request->queryParams);
    }

    /**
     * @return Task|null
     * @throws InvalidConfigException
     */
    public function actionCreate(): ?Task
    {
        $model = new Task([
            'scenario' => Task::SCENARIO_CREATE,
        ]);

        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->save()) {
            return $model;
        }

        $this->actionSuccess = false;
        $this->actionErrors  = $model->getErrorSummary(true);

        return null;
    }

    /**
     * @param int $id
     *
     * @return Task|null
     * @throws InvalidConfigException|NotFoundHttpException
     */
    public function actionUpdate(int $id): ?Task
    {
        $model           = $this->findModel($id);
        $model->scenario = Task::SCENARIO_UPDATE;

        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->save()) {
            return $model;
        }

        $this->actionSuccess = false;
        $this->actionErrors  = $model->getErrorSummary(true);

        return null;
    }

    /**
     * @param int $id
     *
     * @return Task|null
     * @throws NotFoundHttpException
     */
    public function actionMarkAsDone(int $id): ?Task
    {
        $model = $this->findModel($id);

        if ($model->canMarkedDone() && $model->markToDone() && $model->save()) {
            $this->actionMessages = ['Task marked as completed.'];

            return $model;
        }

        $this->actionMessages = ['The task has uncompleted subtasks or has already been completed.'];
        $this->actionErrors   = $model->getErrorSummary(true);
        $this->actionSuccess  = false;

        return null;
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws Throwable
     * @throws StaleObjectException
     * @throws NotFoundHttpException
     */
    public function actionDelete(int $id): bool
    {
        $model = $this->findModel($id);
        if ($model->status === Task::STATUS_DONE || !empty($model->children)) {
            $this->actionMessages = ['Task completed or has sub-tasks.'];
            $this->actionSuccess  = false;

            return false;
        }

        if (!$model->delete()) {
            $this->actionMessages = ['Delete task failed.'];
            $this->actionSuccess  = false;

            return false;
        }

        $this->actionMessages = ['Task deleted successfully.'];

        return true;
    }

    /**
     * @param int $id
     *
     * @return Task
     * @throws NotFoundHttpException
     */
    protected function findModel(int $id): Task
    {
        $model = Task::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException('Task not found.');
        }

        return $model;
    }

    /**
     * {@inheritdoc}
     */
    protected function verbs()
    {
        $verbs = parent::verbs();
        unset($verbs['view']);

        return array_merge($verbs, [
            'mark-as-done' => ['POST'],
        ]);
    }
}
