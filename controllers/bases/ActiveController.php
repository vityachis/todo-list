<?php

namespace app\controllers\bases;

use app\models\User;
use yii\db\ActiveRecord;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\rest\ActiveController as BaseActiveController;
use yii\web\Response;

/**
 * Class ActiveController
 *
 * @property ActiveRecord $modelClass
 */
class ActiveController extends BaseActiveController
{
    /**
     * @var bool
     */
    public $actionSuccess = true;

    /**
     * @var array
     */
    public $actionErrors = [];

    /**
     * @var array
     */
    public $actionMessages = [];

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'cors'              => [
                'class' => Cors::class,
                'cors'  => [
                    'Origin'                           => ['*'],
                    'Access-Control-Request-Method'    => ['GET', 'POST', 'PUT', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers'   => ['authorization', 'content-type'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age'           => 86400,
                ],
            ],
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class'       => CompositeAuth::class,
                'authMethods' => [
                    [
                        'class' => HttpBasicAuth::class,
                        'auth'  => static function ($username, $password) {
                            $user = User::find()->where(['username' => $username, 'status' => User::STATUS_ACTIVE])->one();
                            if ($user && $user->validatePassword($password)) {
                                return $user;
                            }

                            return null;
                        },
                    ],
                    QueryParamAuth::class,
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        $actions = parent::actions();
        unset($actions['view']);

        return $actions;
    }

    /**
     * {@inheritdoc}
     */
    public function afterAction($action, $result)
    {
        return [
            'success'  => $this->actionSuccess,
            'errors'   => $this->actionErrors,
            'messages' => $this->actionMessages,
            'result'   => parent::afterAction($action, $result),
        ];
    }
}
