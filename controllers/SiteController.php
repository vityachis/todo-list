<?php

namespace app\controllers;

use app\models\forms\SignupForm;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class SiteController
 */
class SiteController extends Controller
{
    /**
     * @var bool
     */
    public $actionSuccess = true;

    /**
     * @var array
     */
    public $actionErrors = [];

    /**
     * @var array
     */
    public $actionMessages = [];

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'cors'              => [
                'class' => Cors::class,
                'cors'  => [
                    'Origin'                           => ['*'],
                    'Access-Control-Request-Method'    => ['GET', 'POST', 'PUT', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers'   => ['authorization', 'content-type'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age'           => 86400,
                ],
            ],
            'contentNegotiator' => [
                'class'   => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function afterAction($action, $result)
    {
        return [
            'success'  => $this->actionSuccess,
            'errors'   => $this->actionErrors,
            'messages' => $this->actionMessages,
            'result'   => parent::afterAction($action, $result),
        ];
    }

    /**
     * @return array
     */
    public function actionIndex(): array
    {
        return [];
    }

    /**
     * @return SignupForm|null
     * @throws \Exception
     */
    public function actionSignup(): ?SignupForm
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post(), '') && $model->signup()) {
            $this->actionMessages = ['Account created successfully.'];

            return $model;
        }

        $this->actionErrors = $model->getErrorSummary(true);

        return null;
    }
}
