<?php

use yii\db\Connection;

return [
    'class'    => Connection::class,
    'dsn'      => 'mysql:host=localhost;dbname=todo_list',
    'username' => 'root',
    'password' => 'root',
    'charset'  => 'utf8',
];
