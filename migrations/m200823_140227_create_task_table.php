<?php

use app\models\Task;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%task}}`.
 */
class m200823_140227_create_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%task}}', [
            'id'           => $this->primaryKey(),
            'user_id'      => $this->integer(11)->notNull(),
            'task_id'      => $this->integer(11)->null(),
            'title'        => $this->string(255)->notNull(),
            'description'  => $this->text()->notNull(),
            'status'       => $this->tinyInteger(1)->notNull()->defaultValue(Task::STATUS_TODO),
            'priority'     => $this->tinyInteger(1)->notNull()->defaultValue(Task::PRIORITY_THREE),
            'date_created' => $this->dateTime()->notNull(),
            'date_updated' => $this->dateTime()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%task}}');
    }
}
