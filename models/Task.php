<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "{{%task}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $task_id
 * @property string $title
 * @property string $description
 * @property int $status
 * @property int $priority
 * @property string $date_created
 * @property string $date_updated
 * @property string $date_done
 *
 * @property User $user
 * @property Task $parent
 * @property Task[] $children
 */
class Task extends ActiveRecord
{
    public $start_priority = self::PRIORITY_ONE;
    public $end_priority   = self::PRIORITY_FIVE;

    public const SCENARIO_INDEX  = '_scenario_index';
    public const SCENARIO_CREATE = '_scenario_create';
    public const SCENARIO_UPDATE = '_scenario_update';

    public const STATUS_TODO = 1;
    public const STATUS_DONE = 2;

    public const PRIORITY_ONE   = 1;
    public const PRIORITY_TWO   = 2;
    public const PRIORITY_THREE = 3;
    public const PRIORITY_FOUR  = 4;
    public const PRIORITY_FIVE  = 5;

    /**
     * @var string[]
     */
    public static $statuses = [
        self::STATUS_TODO => 'TODO',
        self::STATUS_DONE => 'Done',
    ];

    /**
     * @var int[]
     */
    public static $priorities = [
        self::PRIORITY_ONE   => 1,
        self::PRIORITY_TWO   => 2,
        self::PRIORITY_THREE => 3,
        self::PRIORITY_FOUR  => 4,
        self::PRIORITY_FIVE  => 5,
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%task}}';
    }

    /**
     * {@inheritdoc}
     */
    public static function find(): ActiveQuery
    {
        return parent::find()->alias('t')->andWhere(['user_id' => Yii::$app->user->id]);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'date_created',
                'updatedAtAttribute' => 'date_updated',
                'value'              => new Expression('NOW()'),
            ],
            [
                'class'              => BlameableBehavior::class,
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => false,
                'value'              => Yii::$app->user->id,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['title', 'description'], 'required'],
            [['user_id', 'task_id', 'status', 'priority', 'start_priority', 'end_priority'], 'integer'],
            [['status'], 'in', 'range' => array_keys(self::$statuses)],
            [['priority'], 'in', 'range' => array_keys(self::$priorities)],
            [['description'], 'string'],
            [['date_created', 'date_updated', 'date_done'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @return array|array[]
     */
    public function scenarios(): array
    {
        $scenarios                        = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['task_id', 'title', 'description', 'status', 'priority'];
        $scenarios[self::SCENARIO_UPDATE] = ['title', 'description', 'status', 'priority'];

        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id'           => 'ID',
            'user_id'      => 'User ID',
            'task_id'      => 'Task ID',
            'title'        => 'Title',
            'description'  => 'Description',
            'status'       => 'Status',
            'priority'     => 'Priority',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'date_done'    => 'Date Done',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function fields(): array
    {
        return [
            'id',
            'parent_id'    => function () {
                return $this->parent->id ?? null;
            },
            'title',
            'description',
            'status'       => function () {
                return self::$statuses[$this->status ?? self::STATUS_TODO] ?? 'Unknown';
            },
            'priority'     => function () {
                return self::$priorities[$this->priority] ?? self::PRIORITY_THREE;
            },
            'date_created' => function () {
                if ($this->date_created instanceof Expression) {
                    return 'NOW';
                }

                return $this->date_created;
            },
            'date_done' => function () {
                if ($this->date_done instanceof Expression) {
                    return 'NOW';
                }

                return $this->date_done;
            },
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'attributes' => [
                    'date_created',
                    'date_done',
                    'priority',
                ],
            ],
        ]);

        $this->load($params, '');

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status,
            'title'  => $this->title,
        ]);

        $query->andFilterWhere(['between', 'priority', $this->start_priority, $this->end_priority]);

        return $dataProvider;
    }

    /**
     * @return bool
     */
    public function markToDone(): bool
    {
        if ($this->status === self::STATUS_DONE) {
            return false;
        }

        $this->status       = self::STATUS_DONE;
        $this->date_done = new Expression('NOW()');

        return true;
    }

    /**
     * @return bool
     */
    public function canMarkedDone(): ?bool
    {
        $model = $this;
        if (is_array($model->children)) {
            foreach ($model->children as $child) {
                if ($child->status !== self::STATUS_DONE || !$model->markToDone()) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getParent(): ActiveQuery
    {
        return $this->hasOne(__CLASS__, ['id' => 'task_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getChildren(): ActiveQuery
    {
        return $this->hasMany(__CLASS__, ['task_id' => 'id']);
    }
}
